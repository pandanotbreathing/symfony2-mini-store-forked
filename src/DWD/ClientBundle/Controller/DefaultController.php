<?php

namespace DWD\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('DWDClientBundle:Default:index.html.twig', array('name' => $name));
    }
}
