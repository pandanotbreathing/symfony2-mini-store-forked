API Interface
=============

Goods
-----

### GET /goods
    [
      {id: xx, name: yy, price: zz},
      {id: xx, name: yy, price: zz},
      ...
      {id: xx, name: yy, price: zz}
    ]

### GET /goods/{id}
    {id: xx, name: yy, price: zz}

### POST /goods - create new good (w/o id) (optional, may be implemented later)
    {name: yy, price: zz}

### DELETE /goods/{id}

### PUT /goods/{id} - update good (w/o id) (optional, may be implemented later)
    {name: yy, price: zz}

Orders
------

### GET /orders
    [
      {
        id: xx,
        items: [
          { good_id: xx, count: yy },
          { good_id: xx, count: yy },
          ...
          { good_id: xx, count: yy }
        ]
      },
      ...
    ]

### GET /orders/{id}
    {
      id: xx,
      items: [
        { good_id: xx, count: yy },
        { good_id: xx, count: yy },
        ...
        { good_id: xx, count: yy }
      ]
    }

### POST /orders - create new (w/o id)
    {
      items: [
        { good_id: xx, count: yy },
        { good_id: xx, count: yy },
        ...
        { good_id: xx, count: yy }
      ]
    }

### PUT /orders/{id} - update (w/o id)
    {
      items: [
        { good_id: xx, count: yy },
        { good_id: xx, count: yy },
        ...
        { good_id: xx, count: yy }
      ]
    }

### DELETE /orders/{id}


Additional:
-----------

Set count:
### PUT /order/{order_id}/{good_id}
    {
      “count” : integer
    }

Remove product from order
### DELETE /order/product/{order_id}/{good_id}

